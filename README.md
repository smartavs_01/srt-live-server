1.介绍
============

- SRT -live-server(SLS)是一个基于安全可靠传输(SRT)的低延迟的开源直播流服务器。通常情况下,运输的延迟在互联网SLS小于1秒
- SLS只支持MPEG-TS格式的流。
- SLS借鉴了RTMP的URL格式(domain/app/stream_name)，例如:msos.push/live/test
- 如何区分同一流是推流还是拉流?在conf文件中通过设置domain_player/domain_publisher和app_player/app_publisher参数来解析。也就是说“domain_publisher/app_publisher”告知SLS是一个推流请求，
- “domain_player/app_player”告知SLS是一个拉流请求，因此这两个字符组合串是不能相等的
- SLS只能运行在基于linux的操作系统上，如mac、centos或ubuntu等。

**注意：该项目已有3年未更新，我将基于V1.4.9进行后续使用上的修改。**


2.依赖
============

请先安装SRT，系统环境请参考SRT(https://github.com/Haivision/srt)。

3.编译
=======

## 3.1.指定SRT和OPENSSL库的安装路径

- 打开Makefile文件；
- 修改INC_SRTSSL为SRT和SSL的头文件路径 ，如INC_SRTSSL=-I/usr/local/openssl/include/ 
- 修改LIB_SRTSSL为SRT和SSL的库文件路径 ，如LIB_SRTSSL=-L/usr/local/openssl/lib/ 

## 3.2.编译

```bash
make
```

4.使用说明
=====

4.1.帮助信息
------------------
```bash
sls -h
```

4.2.指定配置文件运行
------------------------------
```bash
sls -c sls.conf
```

5.测试
====

5.1.使用ffmpeg进行测试
------------------

**推流指令**

```bash
./ffmpeg -f avfoundation -framerate 30 -i "0:0" -vcodec libx264  -preset ultrafast -tune zerolatency -flags2 local_header  -acodec libmp3lame -g  30 -pkt_size 1316 -flush_packets 0 -f mpegts "srt://[your.sls.ip]:8100?streamid=msos.push/live/test"
```

**播放指令**

```bash
./ffplay -fflags nobuffer -i "srt://[your.sls.ip]:8100?streamid=msos.pull/live/test"
```

**注意：您可以通过FFMPEG推送直播流。编译FFMPEG源码时，需要启用SRT（--enable-libsrt）** 

5.2.使用srt-live-client进行测试
---------------------------

​		在SLS中有一个测试工具，它可以用于性能测试。因为它没有编解码器的开销，主要是主网络开销。SLC可以将SRT流保存为TS文件，也可以将TS文件推送到SRT流。

**TS文件作为输入，推送SRT流**

```bash
./slc -r srt://[your.sls.ip]:8080?streamid=msos.push/live/test -i [TS文件]
```

**SRT流作为输入，保存为TS文件**

```bash
./slc -r srt://[your.sls.ip]:8080?streamid=msos.pull/live/test -o [TS文件]
```

6.发布信息
============

1.2.x
----
1. 修改内存使用模式。在v1.1中，发布者将数据复制到每个播放器；在v1.2中，发布者将数据放入数组，所有播放器从该数组读取数据。
2. 修改发布者和播放器的关系。播放器不是发布者的成员，它们之间唯一的关系是数组数据。
3. 添加推拉功能。支持所有和哈希模式的推，支持循环和哈希的拉。在集群模式下，您可以将流推送到散列节点，并从相同的散列节点拉取该流。
4. 在pull和push的上游支持hostname:port/app

1.3.x
----
1. 支持重载。
2. 为中继添加idle_streams_timeout特性。
3. 将许可证类型从GPL更改为mit。

## 1.4.x

1. 增加HTTP统计信息。
2. 增加HTTP事件通知，on_connect, on_close。
3. 在slc(srt-live-client)工具中添加播放器功能，用于压力测试。
4. 为slc (srt-live-client)工具增加发布功能，它可以根据DTS推送TS文件。
5. 修改主机不可用时的HTTP bug。
6. 将remote_ip和remote_port添加到on_event_url中，这可以作为播放器或发行商的唯一标识。
7. 为了兼容MAC系统，TCP监听由epoll模式更改为select模式
8. 修改HTTP检查重复错误以重新打开。
9. 兼容OBS流，OBS支持srt协议，版本为v25.0以上。(https://obsproject.com/forum/threads/obs-studio-25-0-release-candidate.116067/)
10. 增加 hls 录制功能.
11. 将pid文件路径从“~/”更新为“/opt/soft/sls/”
12. 将pid文件路径从“/opt/soft/sls/”更新为“/tmp/sls”，以避免在某些情况下使用根权限。
13. 兼容SRT v1.4.1，在设置方法之前添加set latency方法
14. 兼容raspberrypi.

## 01.05.01
1. 对文档进行中文化；

2. 支持SRT的加密传输；

## 01.05.02

1. 为了保证程序长期运行，增加看门狗功能；
