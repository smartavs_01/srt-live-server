配置文件的指令描述。

# 1.srt全局配置

## 1.1.性能限制

```
worker_threads
	对于sls工作线程，建议和CPU核数匹配
worker_connections
	每个工作线程的最大客户端连接数，请根据CPU性能设置此值
```

## 1.2.日志打印

```
log_file
	日志文件名，如logs/error.log
log_level
	日志级别,包括trace, debug, info, warning, error 和 fatal;
```

## 1.3.统计信息

```
stat_post_url
	SLS提交统计信息的URL地址，如:http://192.168.31.106:8001/sls/stat
stat_post_interval
	SLS提交统计信息的时间间隔，单位为秒
```

## 1.4.文件录制

```
record_hls_path_prefix
	录制hls的前缀。例如:/tmp/mov/sls;完整的记录路径为:/tmp/mov/sls/$listen/$domain_publisher/$app_publisher/$stream_name，默认的vod记录文件名为:/tmp/mov/sls/$listen/$domain_publisher/$app_publisher/$stream_name/vod。当publisher停止时，将生成vod录制文件和m3u8。
```

# 2.server配置

## 2.1.监听性能

```
listen
	sls的监听端口
backlog
	同时接收的连接数量
idle_streams_timeout
	没有数据的超时时间，当超时时服务器将丢弃客户端，单位秒。-1是表示无超时限制。
```

## 2.2.SRT套接字参数设置

```
latency
	srt的延迟，单位:毫秒
pbkeylen 
	发送方加密密钥长度，以字节为单位。只能设置为0、16、24、32，默认值为0。如果不为0，则启用发送加密。
passphrase  
	HaiCrypt加密/解密密码字符串，长度从10到79字符。密码字符串是发送方和接收方之间的共享秘钥。
	仅当pbkeylen非零时，才有效。只有当接收到的数据被加密时，它才会在接收方上使用。配置的密码字符串不能恢复(只写)。
enforced_encryption
	如果为true，连接双方必须设置相同的密码(包括空，即没有加密)。如果密码不匹配或只有一方未加密，则拒绝连接，默认为true。
```

## 2.3.域名配置

```
domain_player
	播发域名可以多于一个，用空格分隔，例如:msos.pull srt.msos.pull;
domain_publisher
	发布域名必须是唯一的，例如:uplive.sls.com
```

## 2.4.事件报告

```
on_event_url
	SLS将on_connect和on_close事件发送到HTTP管理器服务器，post url的语法如下:“http://192.168.31.106:8000/sls/on_event?method=on_connect|on_close&role_name=%s&srt_url=%s&remote_ip=%s&remote_ip=% s&remote_ip=%d”。
	在http管理器服务器中，我们可以通过角色名、srt_url、远程ip和远程端口检查连接是否正常，如果返回的不是http响应码200 OK，则连接将被拒绝。我们还可以记录连接的开始时间和结束时间。
```

# 3.app配置

## 3.1.应用名称

```
app_player
	播放器应用程序名称，SRT的url和rtmp类似，例如:SRT://domain/live/stream_name;
app_publisher
	发布者的应用程序名称;
```

## 3.2.录制文件

```
record_hls
	是否启用HLS录制，可取值on或off，默认是off。
record_hls_segment_duration
	录制HLS的切片时长，单位秒，默认10s
```

# 4.relay配置

```
type
	支持pull和push两种类型
mode
	支持loop和hash两种模式。在loop模式下，SLS将依次循环连接上游的url，直到连接成功；在hash模式下，SLS将使用url哈希值连接上行服务器。
reconnect_interval
	连接失败时重新连接间隔，单位秒;
idle_streams_timeout
	没有数据的超时时间，当超时时服务器将丢弃客户端，单位秒。-1是表示无超时限制。
upstreams
	relay上游流的url地址，如果有多个，用空格分隔，例如:
	“127.0.0.1:9090?streamid=live.sls.com/live 192.168.1.100:8080/?streamid=live.test.com/live”
```

